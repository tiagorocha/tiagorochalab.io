---
title:  "Ansible"
date:
tags:
categories:
---

## Com o PyPI Instalado (eg. apt install python3-pip)

```console
sudo pip install ansible ansible-lint
```

## Instalar Dependências

```console
sudo pip install paramiko PyYAML Jinja2 httplib2 six netaddr
sudo apt install sshpass
```

## Criar Diretórios

```console
sudo mkdir -p /etc/ansible/{roles,keys}
```

## Baixar Arquivo de Configuração Padrão

```console
sudo wget -P /etc/ansible https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg
sudo wget -P /etc/ansible https://raw.githubusercontent.com/ansible/ansible/devel/examples/hosts 
```

## ansible-completion

```console
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-completion.bash
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-doc-completion.bash
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-galaxy-completion.bash
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-playbook-completion.bash
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-pull-completion.bash
sudo wget -P /etc/bash_completion.d https://raw.githubusercontent.com/dysosmus/ansible-completion/master/ansible-vault-completion.bash
```

## Criar chaves SSH

```console
sudo ssh-keygen
```

Localização e nome use algo como `/etc/ansible/keys/sshkey`.

Para copiar a chave pública para um host use algo como:

```console
ssh-copy-id -i /etc/ansible/keys/sshkey.pub localhost 
ssh-copy-id -i /etc/ansible/keys/sshkey.pub ansibleuser@192.168.1.20
```

## Configuração básica do Ansible

Abrir o `ansible.cfg` e descomentar e/ou editar as seguintes linhas:

```txt
    roles_path = /etc/ansible/roles
    timeout = 30
    log_path = /var/log/ansible.log
    private_key_file = /etc/ansible/keys/sshkey
```

## O básico - comandos Ad-Hoc

```console
ansible all -i hosts -u vagrant -m ping
ansible all -i hosts -u vagrant -m setup
ansible webservers -i hosts -u vagrant -m apt -a "name=nginx state=present" -b
ansible webservers -i hosts -u vagrant -m apt -a "name=nginx state=absent" -b
ansible all -b -m user -a "name=suporte state=present shell=/bin/bash password=$1$jqL3N2jW$lnnI9PdSiKZ.JPPwBx.x7/"
```
