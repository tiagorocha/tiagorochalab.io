---
title:  "GNS3 no GNU/Linux"
date:
tags:
categories:
---

## Instalação no Fedora

### Instalar Virt-Manager no Fedora

Instalar solução de virtualização (virt-manager):

```console
sudo dnf install @virtualization
sudo systemctl enable --now libvirtd
sudo systemctl status libvirtd
sudo usermod -a -G libvirt $USER
```

### Instalar GNS3 no Fedora

Instalar o GNS3:

```console
sudo dnf install gns3-server gns3-gui wireshark wireshark-qt
```

## Instalação no Debian/Ubuntu e Derivados

### Instalar Virt-Manager no Debian/Ubuntu e Derivados

Instalar solução de virtualização (virt-manager):

```console
sudo apt install virt-manager virt-viewer qemu-kvm
sudo systemctl status libvirtd
sudo adduser $USER libvirt
sudo adduser $USER libvirt-qemu
```

Como adicionamos o usuário aos grupos do libvirt é necessário fazer um novo login.

Certifique-se de ter a rede default do libvirt ativa:

```console
sudo virsh net-list --all
```

Se for o caso ative e marque o autostart:

```console
sudo virsh net-start default
sudo virsh net-autostart default
```

### Instalar GNS3 no Debian 9

Para criar um arquivo com o repositório do GNS3, depois adicionar as chaves do repo e instalar o GNS3 rodamos os seguintes comandos:

```console
sudo sh -c 'echo "deb http://ppa.launchpad.net/gns3/ppa/ubuntu xenial main" > /etc/apt/sources.list.d/gns3.list'
sudo apt install dirmngr
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F88F6D313016330404F710FC9A2FD067A2E3EF7B
sudo apt update
sudo apt install gns3-gui
```

### Instalar GNS3 no Debian 10 e Testing

Instalar dependências:

```console
sudo apt install dnsmasq vpcs wireshark vinagre python3-pip python3-pyqt5.qtsvg python3-pyqt5.qtwebsockets
```

Compilar e instalar o **ubridge**:

```console
sudo apt install git libpcap-dev
git clone git://github.com/GNS3/ubridge.git
cd ubridge
make
sudo make install
```

Por fim instalar o GNS3:

```console
pip3 install gns3-gui gns3-server
```

### Instalar GNS3 no Ubuntu e Derivados

Para instalar o GNS3 no Ubuntu e distros derivadas, como por exemplo, Linux Mint, Elementary OS, Trisquel, etc, a melhor forma é usando o PPA.

```console
sudo add-apt-repository ppa:gns3/ppa
sudo apt update
sudo apt install gns3-gui
```

Mais detalhes [aqui](https://docs.gns3.com/1QXVIihk7dsOL7Xr7Bmz4zRzTsJ02wklfImGuHwTlaA4/index.html) na documentação oficial do GNS3.
