---
title:  "Django"
date:
tags:
categories:
---

Criar Virtual Environment:

```console
mkdir projeto
cd projeto
python3 -m venv .projeto
```

Ativar Virtual Environment:

```console
source .projeto/bin/activate
```

Instalar Django:

```console
pip install django
```

Criar projeto Django:

```console
django-admin startproject nome_do_projeto .
```

Criar alias para o manage.py:

```console
echo "alias manage='python $VIRTUAL_ENV/../manage.py'" >> .bash_aliases
```

Rodar o projeto Django:

```console
manage runserver
```

Criar Django app chamado core:

```console
manage startapp core
```
