---
title:  "smartmontools"
date:
tags:
categories:
---

Instalação no Debian e derivados:

```console
sudo apt install smartmontools
```

Instalação no Fedora e derivados:

```console
sudo dnf install smartmontools
```

Ativar:

```console
sudo smartctl -s on /dev/sda
```

Diagnóstico Rápido:

```console
sudo smartctl -H /dev/sda
```

Teste Rápido:

```console
sudo smartctl -t short /dev/sda
```

Teste Longo:

```console
sudo smartctl -t long /dev/sda
```

Resultado:

```console
sudo smartctl -l selftest /dev/sda
```
