---
title:  "Hugo"
date:
tags:
categories:
---

## Instalação do Hugo

Instalação no Debian e derivados:

```console
sudo apt install hugo
```

Instalação no Fedora e derivados:

```console
sudo dnf install hugo
```

Para instruções mais completas da instalação do hugo em outros formatos de pacotes e Sistemas Operacionais veja a documentação oficial [aqui](https://gohugo.io/getting-started/installing/).

## Usando o Hugo

Inciando novo projeto:

```console
hugo new site nome_do_projeto
cd nome_do_projeto
```

Aplicando Tema:

```console
hugo mod init github.com/<your_user>/<your_project>
```

Depois configurar o tema "mini" no `config.toml`.

```toml
theme = 'github.com/nodejh/hugo-theme-mini'
```

Lembrando que é necessário ter [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) e [Go](https://go.dev/doc/install) instalados e funcionando.

Novo post:

```console
hugo new posts/outro-post.md
```

Construindo o projeto:

```console
hugo
```

O website é construído no diretório `public` do projeto.

Rodando o servidor web do hugo:

```console
hugo server -D
```

O website fica acessível em http://localhost:1313/ .
