---
title:  "Podman"
date:
tags:
categories:
---

Baixar imagem:

```console
podman pull nome_da_imagem
```

Listar imagens:

```console
podman images
podman image ls
```

Rodar comando em um novo container:

```console
podman run nome_da_imagem [comando]
podman run -it nome_da_imagem [comando]
```

Listar containers:

```console
podman ps
podman container ls
```

Executar comando em um container rodando:

```console
podman exec id_do_container [comando]
```

Iniciar um ou mais containers:

```console
podman start id_do_container
```

Anexar em container rodando:

```console
podman attach id_do_container
```

Remover um ou mais containers:

```console
podman rm id_do_container
```

Remover todos os containers:

```console
podman container rm $(podman ps -aq)
```

Criar um novo volume:

```console
podman volume create --name nome_do_volume
```

Usar volume:

```console
podman run -it -v [nome_do_volume]:[path_no_container] nome_da_imagem
```

Construir imagem baseada em container:

```console
podman commit id_do_container [nome_da_nova_imagem]
```

Construir imagem usando um Containerfile:

```console
podman build -t=nome_da_imagem /path/Containerfile
```

```txt
# Containerfile
FROM debian
RUN apt-get update
RUN apt-get install -y nginx
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
```
