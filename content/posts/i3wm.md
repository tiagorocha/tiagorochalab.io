---
title: "i3wm: Produtividade e Desempenho Com Um Desktop Minimalista"
date: 2019-01-04T11:43:15Z
tags: ["Debian", "Desktop", "Fedora", "i3", "Tiling"]
categories: ["Desktop", "OS"]
---

i3 é um gerenciador de janelas do tipo tiling ("lado a lado") rápido e leve, conhecido pela simplicidade de configuração, customização, ótima documentação e um bom suporte a múltiplos monitores. A configuração do i3 é feita por um arquivo de texto, normalmente **~/.config/i3/config** ou **~/.i3/config**.

## Instalação

Debian:

```console
sudo apt install i3 i3status i3lock xinit x11-xserver-utils xserver-xorg-input-synaptics
```

Fedora:

```console
sudo dnf install i3 i3status i3lock @base-x @standard
```

Devemos verificar e se o modo gráfico está definido como padrão, e se for o caso, definir e iniciá-lo com os seguintes comandos:

```console
systemctl get-default
sudo systemctl set-default graphical.target
sudo systemctl isolate graphical.target
```


## Primeiro Login

Carregar o i3 é bem simples, caso já tenhamos um outro ambiente de desktop instalado podemos usar o gerenciador de sessões para selecionar a sessão i3 na hora do login. Caso contrário podemos logar em modo texto mesmo e rodar `i3` para iniciar uma sessão.

![](/img/i3wm/i3wm-dm-login.png)

No primeiro login o i3 irá perguntar sobre a localização do arquivo de configuração e a modifier key ("tecla de modificação"), que é nossa tecla principal para usar os atalhos do i3.

![](/img/i3wm/i3wm-first-conf.png)

Não vou escrever aqui sobre o básico da usabilidade o i3, acredito que a [documentação oficial](https://i3wm.org/docs/) cumpre muito bem seu papel, além disso é muito fácil encontrar dezenas (talvez centenas) de posts e vídeos de ótima qualidade mostrando o "arroz com feijão" do i3.

## Emulador de Terminal

No momento meu emulador de terminal escolhido é o **sakura** (pacote de mesmo nome), ele é bem leve e têm poucas dependências o que ajuda a minha instalação a se manter enxuta. Para definir qual o emulador de terminal o i3 deve usar por padrão basta editar o arquivo de configuração do i3 deixando como visto abaixo.

```text
# start a terminal
bindsym $mod+Return exec sakura
```

## Aparência Básica do Desktop

### Ícones, Mouse e Cores

Eu gosto de dar uma "tunada" no visual do desktop, isto é, escolher esquema de cores, ícones, decoração das janelas, cursor do mouse e etc. Como as mudanças dessas configurações são raras eu ainda prefiro usar um app com interface gráfica que editar arquivos de configuração. A ferramenta com GUI mais leve e prática que encontrei até o momento para essa tarefa foi o LXAppearance.

```console
sudo {apt | dnf} install lxappearance
```

Para abrir o LXAppearance basta teclar **mod + D**, digitar exatamente **lxappearance** ou **Customize Look and Feel** (**Personalizar Visual** em pt-br) e em seguida teclar **Enter**.

![](/img/i3wm/i3wm-lxappearance.png)

### Papel de Parede

Se você já viu alguns tutoriais de i3 já deve conhecer o **feh**. Ele é um visualizador de imagens super leve que também é usado para definir o papel de parede do desktop. Embora o feh seja muito bom por questão de praticidade (principalmente ao usar múltiplos monitores) e um melhor suporte a imagens SVG eu prefiro o **Nitrogen** para configurar o papel de parede.

```console
sudo {apt | dnf} install nitrogen
```

O único trabalho que tenho no Nitrogen é indicar meus diretórios com imagens para poder carregá-las.

![](/img/i3wm/i3wm-nitrogen.png)

Após escolher uma imagem usando a interface gráfica do Nitrogen precisamos adicionar a linha abaixo no arquivo de configuração do i3.

```text
exec --no-startup-id nitrogen --restore
```

## Aparência Básica do i3

### Bordas das Janelas

O estilo e bordas das janelas são definidas pelas opções de configuração **default_border** e **default_floating_border** (para janelas flutuantes). Os 3 principais estilos são:

- **none**: Sem borda e sem título nas janelas.
- **normal**: Com borda e com título nas janelas.
- **pixel**: Com borda e com título somente quando necessário.

Para normal e pixel o tamanho da borda pode ser definido no final da linha de configuração (como visto abaixo), **0 px** significa que não terá borda.

```text
default_border pixel 3 px
default_floating_border pixel 0 px
```

Quando temos apenas uma janela em um determinado workspace podemos deixar as bordas ocultas até ter outra(s) janela(s), quando realmente é útil ter bordas visíveis.

```text
hide_edge_borders smart
```

### i3status

O i3status é opcional e faz o papel da barra de status. Eu não vou discorrer a cerca do conteúdo do arquivo de configuração, considero que o [manual do i3status](https://i3wm.org/i3status/manpage.html) cubra o necessária para você escrever seu arquivo do zero, e que é muito fácil encontrar exemplos prontos fazendo uma rápida pesquisa na internet.

Com relação ao arquivo de configuração do i3 o exemplo abaixo ilustra uma entrada típica. Inclusive é possível trocar o i3bar escolhido apenas editando uma linha.

- **mode hide**: Barra só fica visível ao pressionar a tecla $mod, ou seja, as janelas podem cobrir a barra.
- **position bottom**: A barra fica na parte inferior da tela, para ficar na parte superior usa-se **top**.
- **font xft:FontAwesome 11**: Define o nome da fonte e tamanho usada na barra, **FontAwesome** com **11** pixels.
- **status_command**: Nesse exemplo definimos um arquivo de configuração específico para nossa barra. O padrão é usar o **~/.config/i3status/config**.
- **colors**: Configuração das cores da barra e seus elementos.

```text
bar {
    mode hide
    position bottom
    font xft:FontAwesome 11
    status_command i3status --config ~/.config/i3/i3status.conf
	
    colors {
        background #000000
        statusline #ffffff

        focused_workspace  #ffffff #285577
        active_workspace   #ffffff #333333
        inactive_workspace #888888 #222222
        urgent_workspace   #ffffff #900000
    }
 }
```

### Tema Rápido e Fácil com j4-make-config

Como pode ser visto no [guia de usuário do i3](https://i3wm.org/docs/userguide.html) configurar as cores pode ser um pouco demorado e trabalhoso. É ai que entra o [j4-make-config](https://github.com/okraits/j4-make-config), ele funciona como uma espécie de gerenciador de temas para o i3.

O j4-make-config é basicamente um script em python que vai substituir variáveis no nosso arquivo de conf do i3 por valores específicos (os temas).

Abaixo temos um exemplo da configuração das cores para as bordas das janelas.

```text
# class                 border  backgr. text    indicator
client.focused          #4c7899 #285577 #ffffff #2e9ef4
client.focused_inactive #333333 #5f676a #ffffff #484e50
client.unfocused        #333333 #222222 #888888 #292d2e
client.urgent           #2f343a #900000 #ffffff #900000
```

Com o j4-make-config usamos a linha abaixo, nossa configuração fica até mais simples.

```text
# $i3-theme-window
```

Veja como fica mais simples o exemplo do i3bar visto anteriormente mas agora usando o j4-make-config para definir as cores:

```text
bar {
    mode hide
    position bottom
    font xft:FontAwesome 11
    status_command i3status --config  ~/.config/i3/i3status.conf

    # $i3-theme-bar

 }
```

#### Obtendo o j4-make-config

```console
git clone https://github.com/okraits/j4-make-config.git
```

Adicionar o script ao PATH do usuário:

```console
PATH="$PATH":"/caminho/para/j4-make-config/"
```

#### Usando o j4-make-config

Precisamos primeiro renomear o arquivo de configuração para **config.base**. Esse passa a ser nosso arquivo de configuração do i3, é ele que devemos editar quando for necessário, o arquivo com nome **config** será gerado automaticamente cada vez que rodamos o j4-make-config. Em seguida editamos o arquivo para ter algo como visto a seguir:

```text
# $i3-theme-window
...
algumas opções de configuração
...
bar {
    ...
    algumas opções de configuração
    ...
    # $i3-theme-bar
}
...
algumas opções de configuração
...
```

Para a ajuda e ver a lista de temas:

```console
j4-make-config -h
```

Definindo um tema e reiniciando o i3:

```console
j4-make-config -r tango-dark
```

## Touchpad

Algumas opções do **synclient** com suas respectivas funções podem ser vistas abaixo. Para uma lista completas de opções basta rodar `synclient -l` no shell.

- **TapButton1=1**: Configura toque com um dedo como botão esquerdo do mouse.
- **TapButton2=3**: Configura toque com dois dedos como clique com o botão direto mouse.
- **TapButton3=2**: Configura toque com três dedos como clique com a scroll do mouse.
- **PalmDetect=1**: Ativa a detecção de palma da mão no touchpad.
- **VertTwoFingerScroll=1**: Ativa rolagem vertical com dois dedos.
- **HorizTwoFingerScroll=1**: Ativa rolagem horizontal com dois dedos.

Linhas no **~/.config/.i3/config**:

```text
exec --no-startup-id synclient TapButton1=1 TapButton2=3 TapButton3=2 PalmDetect=1 VertTwoFingerScroll=1 HorizTwoFingerScroll=1
exec --no-startup-id syndaemon -d -k -i 0.5
```

## Controle do Volume

### App de Controle de Volume

Se assim como eu você curte muito aplicações CLI provavelmente vai ficar bem servido com o ALSAMixer (comando `alsamixer` disponível no pacote **alsa-utils**).

Já se precisa de um mixer com algumas funções a mais e não se importa dele ter uma GUI o PulseAudio Volume Control (pavucontrol) é uma ótima pedida.

```console
sudo {apt | dnf} install alsa-utils pavucontrol
```

### Teclas de Controle de Volume

Abaixo temos um exemplo de configuração das teclas de volume:

```text
bindsym XF86AudioLowerVolume exec amixer sset Master '5%-'
bindsym XF86AudioRaiseVolume exec amixer sset Master '5%+'
bindsym XF86AudioMute exec amixer sset Master toggle
```

## Tela

### Múltiplos Monitores e Resolução

A gerência de monitores e resoluções eu gosto de fazer com o ARandR. Ele é bem leve e têm poucas dependências. Mas ainda assim é opcional porque as mesmas atividades podem ser feitas diretamente no shell com o comando **xrandr**.

```console
sudo {apt | dnf} install arandr
```

### Teclas de Controle do Brilho

Meu controle de brilho da tela é feito pelo **brightnessctl** (pacote de mesmo nome).

```text
bindsym XF86MonBrightnessUp exec sudo brightnessctl set +10%
bindsym XF86MonBrightnessDown exec sudo brightnessctl set 10%-
```

Como o brightnessctl precisa de permissões de super usuário para mudar o brilho da tela eu adiciono a linha abaixo no meu arquivo de configuração do sudo.

```text
nome-do-usuário   ALL=NOPASSWD:/usr/bin/brightnessctl
```

### Bloqueio e Proteção de Tela

Para iniciar o protetor de tela e travar a tela precisamos garantir que temos o **Xautolock** instalado.

```console
sudo {apt | dnf} install xautolock
```

#### i3Locker

Para uma cor sólida com 10 minutos de inatividade para travar a tela:
```text
set $Locker i3lock --color=005577
exec xautolock -time 10 -locker "$Locker" &
```
Usando uma imagem e 3 minutos de inatividade:

```text
set $Locker "i3lock --nofor --color=005577 --image=/caminho/para/imagem.png && sleep 1"
exec xautolock -time 3 -locker "$Locker" &
```

#### XScreenSaver

Caso não queiramos um locker tão minimalista como o i3Locker podemos usar o XScreenSaver. A instalação dele é vista abaixo, o **xscreensaver-*** instala todos os pacotes contendo protetores de tela, interessante se você quer visualizar todos e escolher os seus favoritos.

```console
sudo {apt | dnf} install xscreensaver xscreensaver-*
```

As configurações são definidas na interface do XScreenSaver, no arquivo de configuração do i3 apenas é feita "a chamada" do aplicativo.

```text
set $Locker xscreensaver-command -lock
exec xscreensaver
```

Para abrir o **XScreenSaver** basta teclar **mod + D**, digitar exatamente **xscreensaver-demo** ou **Screensaver** (**Protetor de tela** em pt-br) e em seguida teclar **Enter**.

![](/img/i3wm/i3wm-xscreensaver.png)

## Aplicativos Básicos de Desktop

Para concluir vamos a uma pequena lista com os principais apps que eu uso para compor meu ambiente desktop com i3.

**ScreenGrab**: Captura de Tela (ScreenShot).

Instalação:

```console
sudo {apt | dnf} install screengrab
```
Adicionar no arquivo de conf:

```text
bindsym Print exec screengrab
```

**ClipIt**: Gerenciador de área de transferência.

Instalação:

```console
sudo {apt | dnf} install clipit
```

Para iniciar clipit com a sessão do i3 adicionamos a linha abaixo no arquivo de configuração:

```text
exec clipit
```

**Viewnior**: Visualizador de imagens leve e cheio de recursos.

```console
sudo {apt | dnf} install viewnior
```

**Ranger**: Gerenciador de arquivos modo texto com atalhos de teclado similares ao VI.

```console
sudo {apt | dnf} install ranger
```

**Thunar**: Gerenciador de arquivos leve e com poucas dependências.

Debian:

```console
sudo apt install thunar thunar-volman gvfs-backends
```

Fedora:

```console
sudo dnf install thunar thunar-volman gvfs
```

**Zathura**: Visualizador de documentos PDF e etc com uma interface minimalista e atalhos similares ao VI.

```console
sudo {apt | dnf} install zathura
```

**qutebrowser**: Navegador minimalista focado no uso de teclado com atalhos ao estilo do VI.

```console
sudo {apt | dnf} install qutebrowser
```

**htop**: Gerenciador de processos modo texto.

```console
sudo {apt | dnf} install htop
```

**compton**: Compositor para X11, ele é responsável por permitir o efeito de transparência das janelas, e etc.

```console
sudo {apt | dnf} install compton
```

Adicionar a linha ao arquivo de configuração para iniciar o compton:

```text
exec compton -f --backend glx --vsync opengl
```

**system-config-printer**: Interface gráfica simplificada para configurar o servidor de impressão CUPS.

Debian:

```console
sudo apt install system-config-printer task-print-server
```

Fedora:

```console
sudo dnf install system-config-printer @printing
```
