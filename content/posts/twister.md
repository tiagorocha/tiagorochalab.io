---
title:  "Twister: O Twitter Decentralizado, Criptografado e Livre"
date: 2019-01-02T23:19:18Z
tags: [ "BitTorrent", "Blockchain", "Twister"]
categories: ["Free Software", "Social"]
---

[Twister](http://twister.net.co/) é uma plataforma descentralizada de microblogging baseada nas tecnologias revolucionárias do  [Blockchain](https://pt.wikipedia.org/wiki/Blockchain) e [BitTorrent](https://pt.wikipedia.org/wiki/BitTorrent).

O software foi criado pelo engenheiro da computação brasileiro, Miguel Freitas, que começou o projeto após as revelações de Edward Snowden. Preocupado com os enormes programas de espionagem em massa e com o poder de censura das empresas proprietárias das [mídias sociais](https://pt.wikipedia.org/wiki/M%C3%ADdias_sociais) Miguel projetou uma plataforma descentralizada, resistente a censura e com criptografia de ponta a ponta.

É possível usar o Twister em sistemas operacionais FreeBSD, GNU/Linux, Mac OS, Windows e Android.

## Instalação

Nesse texto vou usar como base as distribuições Debian e Fedora para mostrar o processo instalação a partir do código fonte, imagino que o processo seja idêntico para distribuições derivada e que seja relativamente fácil adaptar para outras distros. 

Instruções completas do processo de instalação nos diversos sistemas operacionais suportados podem ser encontradas no [repositório do projeto](https://github.com/miguelfreitas/twister-core). Instalador Windows e pacotes pré-compilados podem ser encontrados na área de [Download](http://twister.net.co/?page_id=23) do site oficial do Twister.

### Twister a partir dos fontes

#### Instalação das dependências

Para construir o Twister com sucesso nós dependemos de um conjunto de compilador mais bibliotecas de software que precisamos ter instalado na nossa distro. Graças ao trabalha duro dos desenvolvedores da Debian e Fedora obter e instalar todas as dependências é muito rápido e fácil, basta um simples comando no shell.

Debian:

```console
sudo apt install git autoconf libtool build-essential libboost-all-dev libssl-dev libdb++-dev libminiupnpc-dev automake
```

Fedora:

```console
sudo dnf install git autoconf automake gcc-c++ libtool libtorrent boost-*-devel libdb-cxx-devel openssl-devel
```

#### Obtendo os fontes

Por uma questão de organização eu gosto de manter os softwares que eu compilo manualmente dentro de **.local/src** no diretório do meu usuário. Mas isso é apenas uma preferência pessoal, um velho hábito, qualquer outro diretório poderia ser escolhido.

```console
cd ~/.local/src/
git clone https://github.com/miguelfreitas/twister-core.git
```

#### Construindo o Twister

O processo de build pode ser muito demorado dependendo da configuração do computador, por isso é uma ótima oportunidade para dá uma pausa, tomar um café e/ou esticar um pouco as pernas.

```console
cd twister-core
./autotool.sh
./configure
make
```

### Instalando o Twister

A instalação é feita com o make, um script será executado, ele irá copiar os binários, arquivos de configurações, ícones e tudo mais para que possamos contar com Twister como mais um aplicativo no nosso Desktop.

```console
sudo make install
```

A desinstalação é igualmente simples, basta rodar (também no diretório twister-core) a linha de comando `sudo make uninstall`.

#### Instalação e configuração da interface web do Twister

Para usar o Twister precisamos de um cliente. Interagir com o Twister na CLI usando formato JSON não algo tão agradável ou prático.

Lembre-se de substituir **user** e **pwd** por valores de sua preferência.

```console
mkdir ~/.twister
echo -e "rpcuser=user\nrpcpassword=pwd" > ~/.twister/twister.conf
chmod 600 ~/.twister/twister.conf
git clone https://github.com/miguelfreitas/twister-html.git ~/.twister/html
```

## Configuração do Twister

Para abrir o cliente do Twister basta usar o lançador no desktop, que a essa altura deve ter sido "criado" pelo _make install_, ou rodando a linha de comando abaixo e em seguida abrindo http://127.0.0.1:28332/index.html no seu navegador preferido, não esquece que o usuário e senha são os valores definidos anteriormente no **twister.conf**.

```console
./twisterd -rpcuser=user -rpcpassword=pwd -rpcallowip=127.0.0.1
```

### Primeiro acesso

Na primeira tela do cliente vamos de **Desktop**.

![](/img/twister/twister-first-conf.png)

Como acabamos de instalar o Twister somos informados que o blockchain está desatualizado.

![](/img/twister/twister-first-conf-blockchain.png)

Após clicar em **Confirm** é necessário aguardar a atualização do blockchain.

![](/img/twister/twister-first-conf-blockchain-1.png)

### Criando uma conta

Em **Login** basta preencher o campo com o nome de usuário verificando a disponibilidade clicando no botão **Check availability**, havendo disponibilidade basta clicar no botão **Create this nickname**.

![](/img/twister/twister-login-new-user.png)

Não preciso nem falar que é muito importante armazenar em um lugar seguro a chave de 52 caracteres gerada, né!? Lembre-se, o Twister é uma plataforma descentralizada, não existem mecanismos para recuperação de senha, não existem administradores para pedir o reset de senha, perder a **secure key** significa perder acesso a sua conta.

### Importando uma conta

Caso já tenhamos uma conta importá-la é muito simples, basta preencher os campos **secret key** e **nickname** e clicar no botão **Import key**.

![](/img/twister/twister-login-import.png)

### Profile

Em **Profile** opcionalmente fornecemos nossas informações básicas, nome completo, uma pequena descrição/biografia, localização, site pessoal, endereço [Tox](https://tox.chat), endereço [Bitmessage](https://bitmessage.org) e uma Foto.

![](/img/twister/twister-profile.png)

**Cuidado!** É muito fácil visualizar a chave secreta apenas clicando no botão **Secret Key**.

### Options

Clicando no ícone das ferramentas na parte superior direita em seguida em **Options** temos acesso completo as configurações do nosso cliente web do Twister. Dentre outra configurações temos linguagem (pt-br está disponível), tema e notificações (sons e integração com desktop).

![](/img/twister/twister-options.png)

### Network

Em **Network** temos acesso ao status, podemos ver coisas como versão do cliente, se o blockchain está atualizado, consumo de banda e etc. Podemos inclusive encerrar o processo do daemon do Twister nessa tela.

![](/img/twister/twister-network-status.png)

Uma coisa legal que podemos fazer na página **Network** é ativar a **Block Generation**. Que nada mais que uma especia de "mineração de Bitcoins", é por meio dela que a rede do Twister permanece "viva". Se você têm muito poder de processamento pode "emprestar" alguns núcleos do processador para o Twister.

![](/img/twister/twister-network-block-generation.png)

### Mensagens diretas e grupos

Outro recurso legal que vale a pena mencionar é a possibilidade de trocar mensagens direta e em grupos, de forma segura, com criptografia de ponta a ponta.

![](http://twister.net.co/wp-content/uploads/2013/11/screenshot_directmsg1.png)
