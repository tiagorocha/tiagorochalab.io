---
title:  "Criando Uma Sessão Exclusiva Para Um Aplicativo"
date: 2019-01-01T00:00:01Z
tags: ["Debian", "Distros", "Fedora", "GNU/Linux", "Xorg"]
categories: ["Desktop", "OS", "Tutorial"]
---

Imagine que você está fazendo um [quiosque](https://pt.wikipedia.org/wiki/Quiosque_interativo), isso poderia ser útil para permitir a "degustação" da conexão de um ISP ou algum serviço online, nesse exemplo seria interessante que os usuários tivessem acesso apenas ao navegador e nada além disso.

Outro caso útil seria para um gamer hardcore que não quer perder nem um "framezinho" no seu jogo preferido para o ambiente de desktop. Então abrir o game diretamente sobre o servidor gráfico pode ser algo desejável apesar do ganho de performance ser altamente discutível.

## O Ambiente

Vou usar as Distribuições Debian e Fedora nesse texto mas imagino que seja muito fácil de replicar as configurações em outras distribuições [Unix-like](https://pt.wikipedia.org/wiki/Sistema_operacional_tipo_Unix) com poucas adaptações.

## Sem Ambiente de Desktop

Considerando que temos uma "instalação mínima" da distribuição para rodar nosso app precisamos ter uma instalação funcional do servidor gráfico, um gerenciador de janelas e para facilitar nossa vida um gerenciador de sessões.

No Fedora a instalação do servidor gráfico, gerenciador de sessão, gerenciador de janelas e etc é facilitada pelos [grupos de pacotes](https://docs.fedoraproject.org/en-US/fedora/f29/system-administrators-guide/package-management/DNF/#sec-Packages_and_Package_Groups).

```console
sudo dnf group install basic-desktop-environment
```

Embora o Debian não conte com grupos de pacotes, até existem os metapacotes que permitem um funcionamento similar, as relações de dependência estão muito bem "amarradas" e nós podemos instalar tudo que precisamos com uma pequena lista de pacotes.

```console
sudo apt install lightdm openbox
```

Só por garantia após a instalação dos pacotes podemos verificar se o modo gráfico está definido por padrão (o **graphical.target** visto na saída do comando abaixo).

```console
systemctl get-default
graphical.target
```

Podemos definir o modo gráfico como padrão e iniciá-lo com os seguintes comandos:

```console
sudo systemctl set-default graphical.target
sudo systemctl isolate graphical.target
```

### Sessão Exclusiva para o Navegador

No exemplo vamos usar o Mozilla Firefox então precisamos nos certificar que temos ele devidamente instalado.

Fedora:

```console
sudo dnf install firefox
```

Debian:

```console
sudo apt install firefox-esr
```

O próximo passo é criar dois arquivos, um arquivo do tipo XSession para o gerenciador de login e o outro é o script da nossa sessão personalizada.

**/usr/share/xsessions/firefox-session.desktop** com o seguinte conteúdo:

```text
[Desktop Entry]
Name=Firefox-Session
Encoding=UTF-8
Type=XSession
Exec=firefox-session
TryExec=/usr/local/bin/firefox-session
```

**/usr/local/bin/firefox-session** com o seguinte conteúdo:

```bash
#!/bin/sh

# Desativar clique com botão direto e tecla Menu
xmodmap -e "pointer = 1 2 32 4 5 6 7 8 9 10 11 12 13"
xmodmap -e "keycode 135 = "

# Desativar as teclas Control
xmodmap -e "keycode 34 = "
xmodmap -e "keycode 105 = "

# Desativar as teclas Alt
xmodmap -e "keycode 64 = "
xmodmap -e "keycode 108 = "

# Desativar as teclas de função
xmodmap -e "keysym F3 = "
xmodmap -e "keysym F11 = "
xmodmap -e "keysym F12 = "

# Iniciar o gerenciador de janelas
exec openbox &

# Navegador em loop para não ser fechado
while true; do
  exec firefox -url tiagorocha.xyz
done
```

Podemos usar o editor em modo texto GNU Nano para criar e preencher os arquivos, por exemplo, basta rodar no shell `sudo nano /usr/share/xsessions/firefox-session.desktop` para obter a tela do editor com o respectivo arquivo. As vantagens do Nano são que ele vêm instalado por padrão e é muito simples de usar porque na parte inferior da tela temos uma "colinha" dos principais comandos, o **^** indica que devemos pressionar a tecla Ctrl seguida da letra para executar o comando associado aquela combinação.

Após criar o script **firefox-session** é muito importante dar permissão de execução a ele.

```console
sudo chmod +x /usr/local/bin/firefox-session
```

Como última configuração vamos ativar o login automático para nossa sessão customizada. No caso do LightDM podemos fazer isso criando o arquivo **/etc/lightdm/lightdm.conf.d/autologin.conf**. Caso o diretório **/etc/lightdm/lightdm.conf.d/** não exista podemos criá-lo com:

```console
sudo mkdir /etc/lightdm/lightdm.conf.d
```

Conteúdo do **/etc/lightdm/lightdm.conf.d/autologin.conf**:

```text
[Seat:*]
autologin-user = nome_do_usuário
autologin-user-timeout = 0
autologin-session = firefox-session
greeter-session = true
```

Por fim verificamos se o LightDM está ativado, se for necessários ativamos e depois reiniciar o gerenciador de sessão.

```console
systemctl status lightdm.service
sudo systemctl enable lightdm.service
sudo systemctl restart lightdm.service 
```

### Sessão Exclusiva para Games

#### Sessão Exclusiva para o Steam

O usuário que quer rodar um sistema operacional apenas para games provavelmente estará mais bem servido com o [SteamOS](https://store.steampowered.com/steamos/buildyourown). Mas vamos lá, vamos ver como seria uma sessão apenas com um gerenciador de janelas leve e o Steam.

**/usr/share/xsessions/steam-session.desktop**:

```text
[Desktop Entry]
Name=Steam-Session
Encoding=UTF-8
Type=XSession
Exec=/usr/local/bin/steam-session
TryExec=/usr/local/bin/steam-session

```

**/usr/local/bin/steam-session**:

```bash
#!/bin/sh

# Iniciar o gerenciador de janelas
exec openbox &

# Steam em loop para não ser fechado
while true; do
  exec steam -bigpicture
done
```

#### Sessão Exclusiva para o PlayOnLinux

**/usr/share/xsessions/playonlinux-session.desktop**:

```text
[Desktop Entry]
Name=PlayOnLinux-Session
Encoding=UTF-8
Type=XSession
Exec=/usr/local/bin/playonlinux-session
TryExec=/usr/local/bin/playonlinux-session
```

**/usr/local/bin/playonlinux-session**:

```bash
#!/bin/sh

# Iniciar o gerenciador de janelas
exec openbox &

# PlayOnLinux em loop para não ser fechado
while true; do
  exec playonlinux
done
```

Um Game específico poderia ser aberto no script, por exemplo, `playonlinux --run "Name do Game"`, ou diretamente com o Wine, algo como `wine /caminho/para/game.exe`.

Lembrando de dar ao script permissão de execução e se for o caso ativar e/ou reiniciar o LightDM. Exemplo:

```console
sudo chmod +x /usr/local/bin/steam-session
sudo systemctl enable lightdm.service
sudo systemctl restart lightdm.service
```

## Com Ambiente Desktop

Caso já tenhamos uma instalação com ambiente desktop ao invés de uma "instalação mínima" de uma distribuição podemos aproveitar a infraestrutura preexistente.

### Partindo de uma instalação KDE Plasma

Pegando nosso exemplo para uma sessão da Steam. O nosso arquivo **/usr/share/xsessions/steam-session.desktop** seria mantido como visto anteriormente.

O **/usr/local/bin/steam-session** ficaria com o seguinte conteúdo:

```bash
#!/bin/sh

# Iniciar o gerenciador de janelas
exec kwin &

# Iniciar o lançador de apps (Atalho Alt+F2)
exec krunner &

# Steam em loop para não ser fechado
while true; do
  exec steam -bigpicture
done
```

Opcionalmente podemos configurar o login automático. Como o KDE Plasma usa o SDDM como gerenciador de login nós precisamos que o arquivo de configuração **/etc/sddm.conf** contenha as seguintes linhas:

```text
[Autologin]
User=nome_do_usuário
Session=steam-session.desktop
```

### Partindo de uma instalação GNOME Shell

O **/usr/local/bin/steam-session** ficaria com o seguinte conteúdo:

```bash
#!/bin/sh

# Iniciar o gerenciador de janelas
exec mutter &

# Steam em loop para não ser fechado
while true; do
  exec steam -bigpicture
done
```

No GNOME Shell o GDM é gerenciador de sessão padrão, para configurar o login automático podemos criar um arquivo chamado **/etc/gdm/autologin.conf** com o conteúdo abaixo:

```text
[daemon]
AutomaticLogin=nome_do_usuário
AutomaticLoginEnable=True
```
