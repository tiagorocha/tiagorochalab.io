---
title:  "Vagrant + Libvirt + Mutate: Aposentando de Uma Vez Por Todas o VirtualBox"
date: 2019-01-09T17:57:59Z
tags: ["Debian", "Fedora", "GNU/Linux", "Vagrant"]
categories: ["Dev", "Ops", "OS"]
---

Esse post vai para aquele seu amigo usuário de VirtualBox no GNU/Linux que justifica o "mal gosto" com as desculpas que precisa por causa do Vagrant, que não sabe usar outros providers, que a box XYZ só está disponível para VirtualBox, etc.

## Instalação do Vagrant

Instalação com pacotes da distribuição:

```console
sudo {apt | dnf} install vagrant
```

Pacotes da HashiCorp podem ser obtidos no [site oficial do Vagrant](https://www.vagrantup.com/downloads.html).

## Instalação do Libvirt

Debian:

```console
sudo apt install virt-manager virt-viewer qemu-kvm
sudo systemctl status libvirtd
sudo adduser $USER libvirt
sudo adduser $USER libvirt-qemu
```

Fedora:

```console
sudo dnf install @virtualization
sudo systemctl enable --now libvirtd
sudo systemctl status libvirtd
sudo usermod -a -G libvirt $USER
```

Como adicionamos o usuário ao(s) grupo(s) é interessante refazer o login para que a mudança fique "visível" para todas as aplicações. Caso você saiba usar o comando `newgrp` nem precisa ter esse trabalho.

## Plugin Libvirt

Como você provavelmente está cansado de saber o provider padrão do Vagrant é o VirtualBox então para usar ele com Libvirt precisamos instalar um plugin.

```console
vagrant plugin install vagrant-libvirt
```

Caso prefira pode usar o gerenciador de pacotes da distribuição para instalar o plugin.

```console
sudo {apt | dnf} install vagrant-libvirt
```

## Plugin Mutate

Eventualmente podemos nos deparar com alguma box que não conta com o Libvirt como provider. Para contornar esse "problema" temos o plugin Mutate que é capaz de converter as boxes entre diversos providers.
 
```console
vagrant plugin install vagrant-mutate
```

Convertendo uma box a partir do URL:

```console
vagrant mutate http://cloud.centos.org/centos/7/vagrant/x86_64/images/CentOS-7-Vagrant-1505-x86_64-01.box libvirt
```

Convertendo uma box já adicionada:

```console
vagrant mutate deb/squeeze-i386 libvirt
```

Convertendo de um provider específico:

```console
vagrant mutate --input-provider=kvm fedora/29-cloud-base libvirt
```

Para mais opções e possibilidades não deixe de ler a documentação do [Vagrant Mutate](https://github.com/sciurus/vagrant-mutate).

## Trabalhando Com Vagrant/Libvirt

Dependendo das opções usadas no Vagrantfile é possível usar o libvirt como provider sem precisar fazer qualquer edição no arquivo. Iniciando uma VM a partir de um Vagrantfile preexistente:

```console
vagrant up --provider libvirt
```

Adicionando box:

```console
vagrant box add --provider libvirt fedora/29-cloud-base
```

No próximo exemplo vamos criar Vagranfile com o conteúdo abaixo, perceba que já temos o libvirt definido como provider por padrão. Portando basta um `vagrant up` para subir a VM.

```ruby
Vagrant.configure("2") do |config|

  config.vm.box = "fedora/29-cloud-base"
  config.vm.hostname = "fedora"
  
  config.vm.provider "libvirt" do |lv|
    lv.driver = "kvm"
    lv.cpus = 2
    lv.memory = 1024
  end

end
```

A opção **driver** define o hypervisor usado, embora o libvirt suporte diversas soluções de virtualização no momento em que eu escrevo o plugin trabalha apenas _kvm_ e _qemu_. Já as opções **cpus** e **memory** definem respectivamente a quantidade processadores virtuais e a quantidade de memoria RAM em MBytes alocados para a VM que será criada.

Uma lista completa das opções do plugin e suas respectivas descrições está disponível no [repositório do projeto](https://github.com/vagrant-libvirt/vagrant-libvirt).
