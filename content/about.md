---
title: "Sobre mim"
date: "2022-11-21"
---

Meio Engenheiro, meio Hacker, um entusiasta de Software Livre, DevOps e Gestão de Projetos. Especialista em Gestão de TI e Rede de Computadores.
