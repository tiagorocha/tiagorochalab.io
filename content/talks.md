---
title: "Palestras"
date: "2019-09-12"
---

**Título:** [GitLab em 10 Minutos](https://tiagorocha.gitlab.io/talk-gitlab-10-min/)  
**Evento:** [INTERNORTE 2019](https://2019.internorte.com.br/)  
**Data:** 14/09/2019

**Título:** [A Revolução Industrial da TI](https://tiagorocha.gitlab.io/talk-industrial-revolution-of-it/)  
**Evento:** [FADESA TechDay 2019](https://medium.com/@wllmbegot/1%C2%BA-tech-day-fadesa-oficinas-e-palestras-gratuitas-sobre-tecnologia-37dadd8fb015)  
**Data:** 15/06/2019

**Título**: [Primeiros Passos Com Docker](https://devopspbs.gitlab.io/talk-intro-docker/)  
**Evento:** [Meetup DevOpsPBS](https://www.meetup.com/devopspbs/events/260403058/)  
**Data:** 25/05/2019

**Título:** [Conceitos Fundamentais de Software Livre](https://tiagorocha.gitlab.io/talk-software-livre/)  
**Evento:** [FLISoL Parauapebas 2019](https://flisolparauapebas.paralivre.org/)  
**Data:** 27/04/2019

**Título:** [Cloud Computing](https://gitlab.com/devopspbs/meetup-cloud-iot/raw/master/presentations/talk-cloud.html)  
**Evento:** [Meetup DevOpsPBS](https://www.meetup.com/devopspbs/events/258128479/)  
**Data:** 26/01/2019
