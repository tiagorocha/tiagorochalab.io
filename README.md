# Tiago Rocha Blog

Repositório com o código fonte do meu blog. O blog é provido pelo [Hugo](http://gohugo.io/) com tema [hyde-hyde](https://github.com/htr3n/hyde-hyde).

## Como rodar o projeto

Com o Git devidamente instalado clonar o projeto:

```console
git clone https://gitlab.com/tiagorocha/tiagorocha.gitlab.io.git
```

Com o podman-compose (ou docker-compose) devidamente instalado rodar:

```console
cd tiagorocha.gitlab.io
podman-compose up
```

O website fica acessível em http://localhost:1313/ .

## License

GPLv3 or later.
