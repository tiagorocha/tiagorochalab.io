FROM debian:10

RUN apt -qqy update && \
    apt -qqy install auto-apt-proxy && \
    apt -qqy install hugo git && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

EXPOSE 1313 1313

COPY ./content /app/content
COPY ./static /app/static
COPY ./config.toml /app/config.toml
RUN git clone https://github.com/htr3n/hyde-hyde.git /app/themes/hyde-hyde
COPY ./static/img/favicon-32.png /app/themes/hyde-hyde/static/favicon.png
RUN hugo

CMD ["hugo", "server", "--bind", "0.0.0.0"]
